require 'json'

$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "sbb/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "sbb"
  s.version     = Sbb::VERSION
  s.authors     = ["Tobias Grasse"]
  s.email       = ["tg@glancr.de"]
  s.homepage    = "https://glancr.de/modules/sbb"
  s.summary     = "mirr.OS data source that fetches SBB departure information from transport.opendata.ch."
  s.description = "Current departures for any SBB station. | Logo: © SBB CFF FFS"
  s.license     = "MIT"
  s.metadata    = { 'json' =>
                {
                  type: 'sources',
                  title: {
                    enGb: 'SBB Departures',
                    deDe: 'SBB Abfahrten',
                    frFr: 'Départs CFF',
                    esEs: 'Salidas de SBB',
                    plPl: 'Wyjazdy SBB',
                    koKr: 'SBB 출발'
                  },
                  description: {
                    enGb: s.description,
                    deDe: 'Aktuelle Abfahrten an Haltestellen der SBB. | Logo: © SBB CFF FFS',
                    frFr: 'Départs actuels pour toutes les gares CFF. | Sigle: © SBB CFF FFS',
                    esEs: 'Salidas actuales para cualquier estación SBB. | Logotipo: © SBB CFF FFS',
                    plPl: 'Aktualne odloty dla każdej stacji SBB. | Logo: © SBB CFF FFS',
                    koKr: 'SBB에서 현재 출발합니다. | 로고: © SBB CFF FFS'
                  },
                  groups: ['public_transport'],
                  compatibility: '0.13.0'
                }.to_json
              }

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "httparty", "~> 0.18"
  s.add_development_dependency 'rails'
end
