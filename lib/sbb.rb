# frozen_string_literal: true

require 'sbb/engine'
require 'httparty'

module Sbb
  class Hooks
    REFRESH_INTERVAL = '5m'
    SBB_API_HOST = 'https://transport.opendata.ch/v1'

    # @return [String]
    def self.refresh_interval
      REFRESH_INTERVAL
    end

    # @param [Hash] configuration
    def initialize(instance_id, configuration)
      @instance_id = instance_id
      @conf = configuration
    end

    def default_title
      @conf['stationName']
    end

    def configuration_valid?
      # TODO: Activate once limit is configurable
      # return false unless @conf['limit'] <= 50
      res = HTTParty.head("#{SBB_API_HOST}/stationboard?station=#{@conf['stationId']}&limit=1")
      res.success?
    rescue HTTParty::ResponseError => e
      false
    end

    def list_sub_resources
      [
        [@conf['stationId'], @conf['stationName']]
      ]
    end

    def fetch_data(_group, _sub_resources)
      station = PublicTransport.find_or_initialize_by(id: @conf['stationId']) do |station|
        station.id = @conf['stationId']
        station.station_name = @conf['stationName']
      end

      station.departures.each { |s| s.mark_for_destruction if s.departure < Time.current }

      fields = %i[to name category number stop/platform stop/delay stop/departure stop/departureTimestamp]
      fields_query = fields.map { |f| "&fields[]=stationboard/#{f}" }.join('')
      transit_type_query = if @conf['chosenTransitTypes'].length.eql? 5
                             nil
                           else
                             @conf['chosenTransitTypes'].map { |f| "&transportations[]=#{f}" }.join('')
                           end
      res = HTTParty.get(
        "#{SBB_API_HOST}/stationboard?station=#{@conf['stationId']}&limit=15#{fields_query}#{transit_type_query}"
      )
      # TODO: use #{@conf['limit']} once implemented

      res['stationboard'].map do |entry|
        uid = Digest::MD5.hexdigest "#{entry['stop']['departureTimestamp']}-#{entry['name']}"
        attributes = {
          departure: entry['stop']['departure'], # Departure time won't change, only to set it on creation
          line: entry['name'] || entry['number'],
          direction: entry['to'],
          transit_type: entry['category'],
          delay_minutes: entry['stop']['delay'],
          platform: entry['stop']['platform']
        }
        station.update_or_insert_child(uid, attributes)
      end
      [station] # Hook fetch_data needs to return an array
    end
  end
end
