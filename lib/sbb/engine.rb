module Sbb
  class Engine < ::Rails::Engine
    isolate_namespace Sbb
    config.generators.api_only = true
  end
end
